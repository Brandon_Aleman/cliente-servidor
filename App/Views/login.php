<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>Fundidora</title>
</head>
<body style="background-color: black">
<div class="col-md-12" align="center">
    <div class="col-md-12">
        <div class="row" class="container" >
            <div class="col-xs-1 col-sm-2 col-md-2" >
                <br><br>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8" align="center" style="background-color: black">
                <img class="pull-left" width=20%"
                     src="https://cocteleria.barmanshop.com/bartender/78-superlarge_default/exprimidor-vertical-de-citrinos-reforzado-aluminio.jpg"
                     alt="Logo"> <br>
                <svg width="5em" style="color: whitesmoke" height="10em" viewBox="0 0 16 16" class="bi bi-person-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path d="M13.468 12.37C12.758 11.226 11.195 10 8 10s-4.757 1.225-5.468 2.37A6.987 6.987 0 0 0 8 15a6.987 6.987 0 0 0 5.468-2.63z"/>
                    <path fill-rule="evenodd" d="M8 9a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                    <path fill-rule="evenodd" d="M8 1a7 7 0 1 0 0 14A7 7 0 0 0 8 1zM0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8z"/>
                </svg>
                <h1 style="color: whitesmoke"> Fundidora </h1>
                <br>
                <br><br>
            </div>
            <div class="col-xs-1 col-sm-2 col-md-2" >
            </div>
            <div class="col-md-12">
                <div class="col-md-12" style="background-color: black">
                    <div class="row" class="container">
                        <div class="col-md-12" align="righ">
                            <button type="button" class="btn btn-outline-light btn-lg" data-toggle="modal" data-target="#Iniciar">Iniciar Sesion</button>
                            <button type="button" class="btn btn-outline-light btn-lg" data-toggle="modal" data-target="#Registrarme">Registrate</button>
                            <br><br><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Iniciar Sesion -->
<div class="modal fade" id="Iniciar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Iniciar Sesion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
               <form method="post" action="">
                 <div class="modal-body">
                     <div style="align-content: center">
                         <svg width="5em" height="5em" viewBox="0 0 16 16" class="bi bi-person-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                             <path d="M13.468 12.37C12.758 11.226 11.195 10 8 10s-4.757 1.225-5.468 2.37A6.987 6.987 0 0 0 8 15a6.987 6.987 0 0 0 5.468-2.63z"/>
                             <path fill-rule="evenodd" d="M8 9a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                             <path fill-rule="evenodd" d="M8 1a7 7 0 1 0 0 14A7 7 0 0 0 8 1zM0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8z"/>
                         </svg>
                     </div>
                     <label class="col-form-label">Correo electronico:</label><br>
                     <input class="form-control input-lg" type="email" placeholder="Correo@ejemplo.com" required>
                     <br>
                     <label class="col-form-label input-lg">Contraseña:</label><br>
                     <input class="form-control" type="password" placeholder="Contraseña" required>
                 </div>
                 <div align="center">
                     <button type="submit" class="btn btn-success btn-lg" data-dismiss="modal">Entrar</button><br><br>
                     <a href=""><h6>¿Olvidaste tu contraseña?</h6></a>
                     <br>
                 </div>
               </form>
        </div>
    </div>
</div>
<!-- Modal Registrarme -->
<div class="modal fade" id="Registrarme" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabe">Registrarte</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div align="center">
                </div>
            <form action="" method="post">
                <label class="col-form-label input-lg">Nombre:</label><br>
                <input name="nombre" class="form-control" type="text" placeholder="Nombre" required>
                <label class="col-form-label input-lg">Apellidos :</label><br>
                <input name="apellidoP" class="form-control" type="text" placeholder="Apellido Paterno" required>
                <input name="apellidoM" class="form-control" type="text" placeholder="Apellido Materno" required>
                <label class="col-form-label">Correo electronico:</label><br>
                <input name="correo" class="form-control input-lg" type="email" placeholder="Correo@ejemplo.com" required>
                <label class="col-form-label input-lg">Direccion :</label><br>
                <input name="direccion" class="form-control" type="text" placeholder="Direccion" required>
                <label class="col-form-label input-lg">Contraseña:</label><br>
                <input name="contrasenia" class="form-control" type="password" placeholder="Contraseña" required>
                <label class="col-form-label input-lg">Confirmar Contraseña:</label><br>
                <input name="confirma" class="form-control" type="password" placeholder="Confirmar Contraseña" required>
               </div>
               <div align="center">
                <button type="submit" class="btn btn-success btn-lg">Registrate</button><br><br>
                   </form>
            </div>
        </div>
    </div>
</div>
<?php
include 'App/Modelos/Conexion.php';
$obj = new Conexion();
$obj -> registar();
?>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>
